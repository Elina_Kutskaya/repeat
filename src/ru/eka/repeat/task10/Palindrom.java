package ru.eka.repeat.task10;

/**
 * Класс для проверки является ли число палиндромом.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Palindrom {
    public static void main(String[] args) {
        int[] palindrome = {1, 2, 3, 3, 2, 1};
        int[] palindrom = {4, 5, 6, 1, 8, 9};
        isPalindrome(palindrome);
        isPalindrome(palindrom);
    }

    /**
     * Метод проверяет, является ли число палиндромом
     *
     * @param palindrome число
     * @return true/false
     */
    private static boolean isPalindrome(int[] palindrome) {
        boolean result = true;
        for (int i = 0; i < (palindrome.length + 1) / 2 && result; i++)
            result = palindrome[i] == palindrome[palindrome.length - i - 1];
        System.out.println(result ? "Это полиндром" : "Это не полиндром");
        return false;
    }
}