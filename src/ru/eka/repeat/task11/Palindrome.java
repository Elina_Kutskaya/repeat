package ru.eka.repeat.task11;

import java.util.Scanner;

/**
 * Класс для определения является ли строка палиндромом.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Palindrome {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        String text = scan.next();
        System.out.println(isPalindrome(text));

    }

    /**
     * Метод определяет является ли введенная строка палиндромом
     *
     * @param text строка
     * @return true/false
     */
    private static boolean isPalindrome(String text) {
        for (int i = 0; i < text.length() / 2; ++i) {
            if (text.charAt(i) != text.charAt(text.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
}