package ru.eka.repeat.task1;

import java.util.Scanner;

/**
 * Класс считывает символы пока не встретится точка.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Symbol {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        String symbol = scan.nextLine();
        int i = symbol.indexOf('.');
        int probel = 0;
        char[] arrayChar = symbol.toCharArray();
        for (int j = 0; j < arrayChar.length; j++) {
            if (arrayChar[j] == ' ') {
                probel++;
            }
        }

        System.out.println("Символов: " + i + " Пробелов: " + probel);
    }
}