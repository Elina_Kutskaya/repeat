package ru.eka.repeat.task6;

import java.util.Scanner;

/**
 * Класс для вывода таблицы умножения введенного пользователем числа.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Table {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        int table = scan.nextInt();
        int[] multiplication = new int[11];

        for (int i = 1; i <= 10; i++) {
            multiplication[i] = table * i;
            System.out.println(table + " * " + i + " = " + multiplication[i]);
        }
    }
}