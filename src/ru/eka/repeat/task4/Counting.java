package ru.eka.repeat.task4;

import java.util.Scanner;

/**
 * Класс для расчета расстояния до места удара молнии.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Counting {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        double v = 1234.8;
        System.out.println("Введите интервал времени.Например - 6,8. ");
        double t = scan.nextDouble();
        double s = v * t;
        System.out.println("Расстояние до места удара молнии " + s);
    }
}
