package ru.eka.repeat.task14;

import java.util.Scanner;

/**
 * Класс определяет является ли символ введенный с клавиатуры цифрой,
 * буквой или знаком пунктуации.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Define {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символ: ");
        String symbol = sc.next();
        char symb = symbol.charAt(0);
        if (Character.isDigit(symb)) {
            System.out.println("Это цифра!");
        }
        if (Character.isLetter(symb)) {
            System.out.println("Это буква!");
        }
        if (".,:;".contains(symbol)) {
            System.out.println("Это пунктуация!");
        }
    }
}
