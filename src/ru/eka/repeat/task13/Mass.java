package ru.eka.repeat.task13;
import java.util.Scanner;

/**
 * Класс для заполнения двумерного массива случайными числами
 * и перенесения построчно этих чисел в одномерный массив.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Mass {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String args[]) {
        System.out.println("Введите кол-во строк и столбцов: ");
        int n = scan.nextInt();
        int[][] mas = new int[n][n];
        int[] mast = new int[n * n];
        filling(mas);

        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mast[i * n + j] = mas[i][j];
                System.out.print(mast[i * n + j] + " ");
            }
        }
    }

    /**
     * Метод для заполнения массива
     *
     * @param mas массив
     */
    private static void filling(int[][] mas) {
        for (int i = 0; i < mas.length; ++i) {
            for (int j = 0; j < mas.length; ++j) {
                mas[i][j] = (int) (Math.random() * 20);
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }
}