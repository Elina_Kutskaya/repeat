package ru.eka.repeat.task8;

import java.util.Scanner;

/**
 * Класс для проверки является ли число типа double целым.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Check {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        double number = scan.nextDouble();

        isInteger(number);
    }

    /**
     * Метод проверяет, является ли заданное число целым
     *
     * @param number введенное число
     */
    private static void isInteger(double number) {
        if (number % 1 == 0) {
            System.out.println("Число целое");
        } else {
            System.out.println("Число не является целым");
        }
    }
}