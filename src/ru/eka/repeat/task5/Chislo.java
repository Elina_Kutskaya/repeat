package ru.eka.repeat.task5;

import java.util.Scanner;

/**
 * Класс для вывода простых чисел в пределах от 2 до 100.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Chislo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        final int N = 100;
        int a[] = new int[N];
        for (int i = 2; i < N; i++) {
            a[i] = 1;
        }
        for (int i = 2; i < N; i++)
            if (a[i] == 1)
                for (int j = i; j * i < N; j++) {
                    a[i * j] = 0;
                }
        for (int i = 2; i < N; i++)
            if (a[i] == 1)
                System.out.println(i + " простое число");
    }
}