package ru.eka.repeat.task3;

import java.util.Scanner;

/**
 * Класс для перевода рублей в евро по заданному курсу.
 * В качестве аргументов количество рублей и курс.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Remittance {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите курс евро: ");
        double course = scan.nextDouble();
        System.out.println("Введите кол-во рублей: ");
        double count = scan.nextDouble();
        System.out.printf("У вас " + convert(count, course) + " евро");


    }

    /**
     * Метод для перевода валют по заданному курсу
     *
     * @param count кол-во денег
     * @param course курс
     * @return перевод валют
     */
    private static double convert(double count, double course) {
        return count / course;
    }
}