package ru.eka.repeat.task12;

import java.util.Scanner;

/**
 * Класс для подсчета количество часов, минут и секунд в n-ном количестве суток.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class CountingTime {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во дней: ");
        int days = scan.nextInt();

        time(days);
    }

    /**
     * Метод для подсчёта и вывода кол-ва часов, минут и секунд в n-ном кол-ве суток
     *
     * @param days кол-во дней
     */
    private static void time(int days) {
        if (days <= 0) throw new IllegalArgumentException();
        System.out.println("В " + days + " сутках: " + days * 24 + " часа, " + days * 1440 + " минут, " + days * 86400 + " секунд.");
    }
}
