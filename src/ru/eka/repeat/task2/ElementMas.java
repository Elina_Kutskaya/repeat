package ru.eka.repeat.task2;

import java.util.Scanner;

/**
 * Класс увеличивает заданный элемент массива на 10%.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class ElementMas {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во элементов: ");
        int n = scan.nextInt();
        double [] mas = new double [n];
        System.out.println("Введите номер элемента: ");
        int chislo = scan.nextInt();

        for (int i = 0; i <mas.length; i++) {
            mas[i] = (int)(Math.random()*(40));
            System.out.print(mas[i] + " ");
        }

        mas[chislo] = mas[chislo] + mas[chislo]*0.1;
        System.out.println();
        System.out.println(mas[chislo]);

    }
}