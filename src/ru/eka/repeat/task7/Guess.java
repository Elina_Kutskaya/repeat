package ru.eka.repeat.task7;

import java.util.Scanner;

/**
 * Простая игра основанная на угадывании чисел.
 * Пользователь должен угадать загаданное число введя его в консоль.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */
public class Guess {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Здравствуй! Я хочу поиграть с тобой!");
        System.out.println("Угадай число которое я загадала!");
        System.out.println("Вводи:  ");
        int secret = (int) (Math.random() * 10);
        int number;
        do {
            number = scan.nextInt();
            if (secret == number) {
                System.out.println("Вы угадали!!!");
            } else {
                if (number > secret) {
                    System.out.println("Загаданное число меньше!");
                } else if (number < secret) {
                    System.out.println("Загаданное число больше!");
                }
            }
        } while (number != secret);
    }
}
